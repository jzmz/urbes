Firenze color scheme
====================

Prendendo inspirazione da la Cattedrale di Santa Maria del Fiero, al centro de la citta di Firenze, questo colori schema e basato su ci colori principali, come el verdi, bianco e tinte di roso.

Inspired by the Cattedrale di Santa Maria del Fiore, in the heart of Florence, this color schema is based heavily in its main colors, which are green, white and tints of red.
